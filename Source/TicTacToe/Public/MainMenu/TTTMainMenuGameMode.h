// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TTTMainMenuGameMode.generated.h"

class UTTTMainMenu;

UCLASS()
class TICTACTOE_API ATTTMainMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ATTTMainMenuGameMode();

	void NewGame();

protected:
	UPROPERTY(EditDefaultsOnly, Category="GameLevel")
	TSoftObjectPtr<UWorld> GameLevel;

	UPROPERTY(EditDefaultsOnly, Category="Main Menu")
	TSubclassOf<class UUserWidget> MainMenuClass;

	UPROPERTY()
	UTTTMainMenu* MainMenu;

	virtual void BeginPlay() override;
};
