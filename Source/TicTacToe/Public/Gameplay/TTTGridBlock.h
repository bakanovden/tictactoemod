// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TTTGridBlock.generated.h"

class ATTTGameplayGameMode;
enum class EFigureType : uint8;
class USceneComponent;
class UStaticMeshComponent;
class UMaterialInterface;
class UStaticMesh;
class ATTTGrid;

/** A block that can be clicked */
UCLASS(minimalapi)
class ATTTGridBlock : public AActor
{
	GENERATED_BODY()

public:
	ATTTGridBlock();

	UFUNCTION()
	void BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked);

	bool LockBlock(EFigureType FigureType);

	void HighlightOn();

	void HighlightOff();

	FORCEINLINE USceneComponent* GetDefaultScene() const { return DefaultScene; }
	FORCEINLINE UStaticMeshComponent* GetBlockMesh() const { return BlockMesh; }
	FORCEINLINE EFigureType GetLockFigureType() const { return LockFigureType; }
	FORCEINLINE bool GetIsLock() const { return bIsLock; }
	UFUNCTION(BlueprintCallable)
	FORCEINLINE int32 GetBlockIndex() const { return BlockIndex; }

	void SetBlockIndex(int32 NewBlockIndex);

	void SetOwningGrid(ATTTGrid* NewOwningGrid);

	void SetGameMode(ATTTGameplayGameMode* NewGameMode);

protected:

	UPROPERTY()
	ATTTGrid* OwningGrid;

	UPROPERTY()
	ATTTGameplayGameMode* GameMode;
	
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	USceneComponent* DefaultScene;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Components")
	UStaticMeshComponent* BlockMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Block")
	UMaterialInterface* BaseMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Block")
	UMaterialInterface* HighlightAvailable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Block")
	UMaterialInterface* HighlightNotAvailable;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Shapes")
	TMap<EFigureType, UStaticMesh*> MeshesByFigureType;

	/** StaticMesh component for selected figure (square/cross/circle and etc.) */
	UPROPERTY()
	UStaticMeshComponent* BlockLockMesh;

	virtual void BeginPlay() override;

private:
	UPROPERTY()
	int32 BlockIndex;

	bool bIsLock;

	EFigureType LockFigureType;

	UPROPERTY()
	AActor* LockInstigator;
};
