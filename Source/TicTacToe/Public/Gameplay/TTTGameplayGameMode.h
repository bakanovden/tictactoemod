#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TTTGameplayGameMode.generated.h"

class ATTTPawn;
class ATTTGrid;
class ATTTTurnManager;
class UTTTSelectFigureMenu;
class UTTTEndGameMenu;
class UTTTPlayerHUD;
enum class EFigureType : uint8;

UCLASS(minimalapi)
class ATTTGameplayGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATTTGameplayGameMode();

	void PlayerSelectedFigure(EFigureType FigureType);

	void PlayerEndTurn();

	void StartGame();

	void WinGame(EFigureType WinFigure);

	void DrawGame();

	void TryReopenEndGameMenu();

	void NewGame();

	void SetGrid(ATTTGrid* NewGrid);

	FORCEINLINE int32 GetCountBlockForWin() const { return CountBlockForWin; }
	FORCEINLINE ATTTTurnManager* GetTurnManager() const { return TurnManager; }
	FORCEINLINE EFigureType GetPlayerFigure() const { return PlayerFigure; }

protected:
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UTTTSelectFigureMenu> SelectFigureMenuClass;
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UTTTEndGameMenu> EndGameMenuClass;
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<UTTTPlayerHUD> PlayerUIClass;
	UPROPERTY(EditDefaultsOnly, Category="GameLevel")
	TSoftObjectPtr<UWorld> GameLevel;

	UPROPERTY(EditDefaultsOnly, Category="Game Rules")
	int32 CountBlockForWin;

	UPROPERTY()
	UTTTSelectFigureMenu* SelectFigureMenu;

	UPROPERTY()
	UTTTEndGameMenu* EndGameMenu;

	UPROPERTY()
	UTTTPlayerHUD* PlayerUI;

	UPROPERTY()
	ATTTGrid* Grid;

	UPROPERTY()
	ATTTTurnManager* TurnManager;

	UPROPERTY()
	ATTTPawn* Player;

	UPROPERTY()
	EFigureType PlayerFigure;

	virtual void BeginPlay() override;
};
