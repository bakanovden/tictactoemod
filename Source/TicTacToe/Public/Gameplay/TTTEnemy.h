// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TTTEnemy.generated.h"

class ATTTGrid;
class ATTTGameplayGameMode;
enum class EDirection : uint8;
enum class EFigureType : uint8;

UCLASS()
class TICTACTOE_API ATTTEnemy : public AActor
{
	GENERATED_BODY()
	
public:
	ATTTEnemy();

	void SetFigureType(EFigureType NewFigureType);

	void Move();

	void SetGrid(ATTTGrid* NewGrid);

	void SetGameMode(ATTTGameplayGameMode* NewGameMode);

protected:

	UPROPERTY()
	ATTTGrid* Grid;

	UPROPERTY()
	ATTTGameplayGameMode* GameMode;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy")
	float ChanceForMoveOnSelectedLine;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enemy")
	float ChanceInterferePlayer;

	EFigureType FigureType;

	int32 SelectedBlockIndex;

	EDirection SelectedDirection;

	TArray<int32> AvailableBlocks;

	virtual void BeginPlay() override;

	bool TryInterferePlayer();

	void RandomPlaceFigure();

	bool CheckAvailableSelectedLine();

	void FindNewLine();
	
	bool CheckAvailableForAnyDirection(int32 BlockIndex);
	
	bool CheckAvailableForDirection(int32 BlockIndex, EDirection Direction);
	
	void GetAllAvailableDirections(int32 BlockIndex, TArray<EDirection>& Direction);
	
	void GetAllAvailablePositions(int32 BlockIndex, EDirection Direction, TArray<int32>& AvailableBlocks);
	
	void GetAllAvailablePositionsFromMiddle(int32 BlockIndex, EDirection Direction, TArray<int32>& AvailableBlocks);

};
