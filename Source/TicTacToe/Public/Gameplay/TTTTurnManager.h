// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TTTTurnManager.generated.h"

class ATTTGameplayGameMode;
class ATTTGrid;
class ATTTPawn;
class ATTTEnemy;

enum class EDirection : uint8;
enum class EFigureType: uint8;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnChangeActivePlayerSignature, EFigureType);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnChangeCountChainsSignature, EFigureType, int32, int32);

UCLASS()
class TICTACTOE_API ATTTTurnManager : public AActor
{
	GENERATED_BODY()

public:
	ATTTTurnManager();

	FOnChangeCountChainsSignature OnChangeCountChains;

	void StartGame();

	void StartNextStageGame();

	void NextMove();

	/**
	 * true = game is end
	 */
	bool UpdateStageGame();

	void AddEnemy(EFigureType FigureType, ATTTEnemy* Enemy);

	FORCEINLINE bool GetIsPlayerMove() const { return bIsPlayerMove; }

	void SetGrid(ATTTGrid* NewGrid);

	void SetGameMode(ATTTGameplayGameMode* NewGameMode);

	void SetPlayerFigure(EFigureType NewPlayerFigure);
protected:

	UPROPERTY()
	ATTTGrid* Grid;

	UPROPERTY()
	ATTTGameplayGameMode* GameMode;

	EFigureType PlayerFigure;

	UPROPERTY()
	ATTTPawn* Player;

	UPROPERTY()
	TMap<EFigureType, ATTTEnemy*> Enemies;

	TArray<EFigureType> OrderMoves;

	int32 CurrentMoveIndex = -1;

	bool bIsPlayerMove;

	UPROPERTY()
	FTimerHandle AIMoveTimerHandle;

	virtual void BeginPlay() override;

	void AIMove();

	bool CheckWinGame();

	bool CheckWinGameInDirection(EDirection Direction);

	void UpdateCountChains();

	bool ContainsNamesFor4Chains(const TSet<FString>& AlreadyChecked, int32 StartIndex, EDirection Direction);

	void AddNamesFor4Chains(TSet<FString>& AlreadyChecked, int32 StartIndex, EDirection Direction);

	bool CheckDrawGame();

	bool CheckAvailableForFigureTypeAnyDirection(EFigureType Figure, int32 BlockIndex);

	bool CheckAvailableInDirectionForFigure(EFigureType Figure, int32 BlockIndex, EDirection Direction);
};
