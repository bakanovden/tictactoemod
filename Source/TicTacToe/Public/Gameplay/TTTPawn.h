#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TTTPawn.generated.h"

UCLASS(config=Game)
class ATTTPawn : public APawn
{
	GENERATED_UCLASS_BODY()

public:
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	virtual void CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult) override;

	void StartTurn();
	void EndTurn();

protected:
	bool bCanMove;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	class ATTTGridBlock* CurrentBlockFocus;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class APlayerController* PlayerController;
	
	virtual void BeginPlay() override;

	void TraceForBlock(const FVector& Start, const FVector& End, bool bDrawDebugHelpers);

	void OpenEndGameMenu();
};
