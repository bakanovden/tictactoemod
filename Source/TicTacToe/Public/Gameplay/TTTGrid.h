#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TTTGrid.generated.h"

class ATTTGridBlock;
class ATTTGameplayGameMode;
enum class EFigureType: uint8;
enum class EDirection: uint8;

UCLASS(minimalapi)
class ATTTGrid : public AActor
{
	GENERATED_BODY()

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Grid")
	class USceneComponent* DefaultRoot;

public:
	ATTTGrid();

	TArray<EDirection> UniqueDirection;

	TMap<EDirection, EDirection> OppositeDirections;

	FORCEINLINE class USceneComponent* GetDefaultRoot() const { return DefaultRoot; }

	void LockBlock(EFigureType FigureType, int32 BlockIndex);

	bool BlockIsFree(int32 BlockIndex);

	void GetBlocks(TMap<int32, ATTTGridBlock*>& Blocks);

	bool CoordinatesFromIndex(int32 BlockIndex, int32& X, int32& Y);

	bool IndexFromCoordinates(int32& BlockIndex, int32 X, int32 Y);

	void DeltaCoordinatesForDirection(EDirection Direction, int32& DeltaX, int32& DeltaY);

	ATTTGridBlock* GetRandomFreeBlock();

	ATTTGridBlock* GetFreeBlock(int32 BlockIndex);

	ATTTGridBlock* GetBlock(int32 BlockIndex);

	ATTTGridBlock* GetLastLockedBlock();

	int32 GetSumLockedBlocks(int32 BlockIndex, EDirection Direction, EFigureType FigureType);

	int32 GetSumFreeBlocksOrLockFigure(int32 BlockIndex, EDirection Direction, EFigureType FigureType);

	int32 GetSumLockedBlocksIgnoreSelf(int32 BlockIndex, EDirection Direction, EFigureType FigureType);

	FORCEINLINE int32 GetSize() const { return Size; }

protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Grid")
	int32 Size;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Grid")
	float BlockSpacing;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Grid")
	TSubclassOf<ATTTGridBlock> BlockClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Grid")
	TMap<int32, ATTTGridBlock*> FreeBlocks;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Grid")
	TMap<int32, ATTTGridBlock*> AllBlocks;

	UPROPERTY()
	ATTTGridBlock* LastLockedBlock;

	UPROPERTY()
	ATTTGameplayGameMode* GameMode;

	virtual void BeginPlay() override;
};



