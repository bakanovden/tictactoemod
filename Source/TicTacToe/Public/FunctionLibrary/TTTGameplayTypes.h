// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "TTTGameplayTypes.generated.h"

UENUM(BlueprintType)
enum class EFigureType : uint8
{
	Square,
	Circle,
	Cross,

	Max
};

UENUM(BlueprintType)
enum class EDirection : uint8
{
	Down,
	Left,
	Right,
	DownLeft,
	DownRight,
	Up,
	UpRight,
	UpLeft,

	Max
};

USTRUCT()
struct FPlayerInfo
{
	GENERATED_BODY()
	int32 Count3InRow;
	int32 Count4InRow;
	EFigureType FigureType;
};

UCLASS()
class TICTACTOE_API UGameplayTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	static void GetFigureTypeName(EFigureType Figure, FString& String);
	
};
