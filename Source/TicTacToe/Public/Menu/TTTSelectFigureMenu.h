// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TTTMenuWidget.h"
#include "TTTSelectFigureMenu.generated.h"

class ATTTGameplayGameMode;
enum class EFigureType : uint8;

UCLASS()
class TICTACTOE_API UTTTSelectFigureMenu : public UTTTMenuWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	void SetGameMode(ATTTGameplayGameMode* MainMenuGameMode);

protected:
	UPROPERTY()
	ATTTGameplayGameMode* GameMode;

private:
	UPROPERTY(meta = (BindWidget))
	class UButton* SelectFigureSquare_Button;
	UPROPERTY(meta = (BindWidget))
	class UButton* SelectFigureCircle_Button;
	UPROPERTY(meta = (BindWidget))
	class UButton* SelectFigureCross_Button;
	UPROPERTY(meta = (BindWidget))
	class UButton* SelectFigureRandom_Button;

	UFUNCTION()
	void ClickSelectSquare();

	UFUNCTION()
	void ClickSelectCircle();

	UFUNCTION()
	void ClickSelectCross();

	UFUNCTION()
	void ClickSelectRandom();

	UFUNCTION()
	void SelectFigure(EFigureType FigureType);
};
