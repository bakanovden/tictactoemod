// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TTTPlayerInfoRow.generated.h"

class UTextBlock;
class ATTTGameplayGameMode;
enum class EFigureType : uint8;

UCLASS()
class TICTACTOE_API UTTTPlayerInfoRow : public UUserWidget
{
	GENERATED_BODY()

public:
	void Setup(EFigureType FigureType);
	void ChangeCountChains(int32 Count3Chains, int32 Count4Chains);

	FORCEINLINE EFigureType GetRowFigure() const { return RowFigure; }

private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* FigureTypeTitle_Text;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* Count3Chains_Text;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* Count4Chains_Text;

	EFigureType RowFigure;
};
