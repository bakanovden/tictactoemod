// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Menu/TTTMenuWidget.h"
#include "TTTEndGameMenu.generated.h"

enum class EFigureType : uint8;
class UButton;
class UTextBlock;
class ATTTGameplayGameMode;

UCLASS()
class TICTACTOE_API UTTTEndGameMenu : public UTTTMenuWidget
{
	GENERATED_BODY()
public:
	virtual bool Initialize() override;

	void SetupPlayerWin();
	void SetupAIWin(const FString& WinnerName);
	void SetupDraw();
	void SetGameMode(ATTTGameplayGameMode* NewGameMode);
	
private:
	UPROPERTY(meta = (BindWidget))
	UButton* Play_Button;
	UPROPERTY(meta = (BindWidget))
	UButton* Quit_Button;
	UPROPERTY(meta = (BindWidget))
	UButton* Hide_Button;
	UPROPERTY(meta = (BindWidget))
	UTextBlock* EngGameStatus_Text;

	UPROPERTY()
	ATTTGameplayGameMode* GameMode;

	UFUNCTION()
	void Play();

	UFUNCTION()
	void Quit();

	UFUNCTION()
	void Hide();
};
