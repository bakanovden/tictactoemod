// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TTTMenuWidget.h"
#include "TTTMainMenu.generated.h"

class UButton;
class ATTTMainMenuGameMode;

UCLASS()
class TICTACTOE_API UTTTMainMenu : public UTTTMenuWidget
{
	GENERATED_BODY()
	
public:
	virtual bool Initialize() override;

	void SetGameMode(ATTTMainMenuGameMode* MainMenuGameMode);

protected:
	UPROPERTY()
	ATTTMainMenuGameMode* GameMode;
	
private:
	UPROPERTY(meta = (BindWidget))
	UButton* Play_Button;
	UPROPERTY(meta = (BindWidget))
	UButton* Quit_Button;

	UFUNCTION()
	void Play();

	UFUNCTION()
	void Quit();
};
