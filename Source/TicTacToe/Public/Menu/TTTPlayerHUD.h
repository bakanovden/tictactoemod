// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TTTPlayerHUD.generated.h"

enum class EFigureType : uint8;
class UTTTPlayerInfoRow;
class UVerticalBox;
class ATTTGameplayGameMode;

UCLASS()
class TICTACTOE_API UTTTPlayerHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	void Setup(ATTTGameplayGameMode* GameMode);

protected:
	UPROPERTY(EditDefaultsOnly, Category="PlayerInfo")
	TSubclassOf<UTTTPlayerInfoRow> RowInfoClass;

	void ChangeCountChains(EFigureType FigureType, int32 Count3Chains, int32 Count4Chains);
private:
	UPROPERTY(meta = (BindWidget))
	UVerticalBox* UIInfoChains_Box;

	UPROPERTY()
	TMap<EFigureType, UTTTPlayerInfoRow*> InfoRows;
};
