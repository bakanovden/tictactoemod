// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/TTTPlayerInfoRow.h"

#include "Components/TextBlock.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTTurnManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerInfoRow, All, All);

void UTTTPlayerInfoRow::Setup(EFigureType FigureType)
{
	RowFigure = FigureType;
	FString RowTitle;
	UGameplayTypes::GetFigureTypeName(FigureType, RowTitle);

	FigureTypeTitle_Text->SetText(FText::FromString(RowTitle));
	ChangeCountChains(0, 0);
}

void UTTTPlayerInfoRow::ChangeCountChains(int32 Count3Chains, int32 Count4Chains)
{
	Count3Chains_Text->SetText(FText::FromString(FString::FromInt(Count3Chains)));
	Count4Chains_Text->SetText(FText::FromString(FString::FromInt(Count4Chains)));
}
