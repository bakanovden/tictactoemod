#include "Menu/TTTMainMenu.h"

#include "Components/Button.h"
#include "Kismet/GameplayStatics.h"
#include "MainMenu/TTTMainMenuGameMode.h"

bool UTTTMainMenu::Initialize()
{
	const bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	if (!Play_Button)
	{
		return false;
	}
	Play_Button->OnClicked.AddDynamic(this, &UTTTMainMenu::Play);

	if (!Quit_Button)
	{
		return false;
	}
	Quit_Button->OnClicked.AddDynamic(this, &UTTTMainMenu::Quit);

	return true;
}

void UTTTMainMenu::SetGameMode(ATTTMainMenuGameMode* MainMenuGameMode)
{
	GameMode = MainMenuGameMode;
}

void UTTTMainMenu::Play()
{
	GameMode->NewGame();
}

void UTTTMainMenu::Quit()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!PlayerController)
	{
		return;
	}

	PlayerController->ConsoleCommand("quit");
}
