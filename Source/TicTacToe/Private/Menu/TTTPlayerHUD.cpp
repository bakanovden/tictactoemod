// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/TTTPlayerHUD.h"

#include "Components/VerticalBox.h"
#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Gameplay/TTTTurnManager.h"
#include "Menu/TTTPlayerInfoRow.h"

DEFINE_LOG_CATEGORY_STATIC(LogPlayerHUD, All, All);

bool UTTTPlayerHUD::Initialize()
{
	return Super::Initialize();
}

void UTTTPlayerHUD::Setup(ATTTGameplayGameMode* GameMode)
{
	AddToViewport();

	if (!RowInfoClass)
	{
		UE_LOG(LogPlayerHUD, Warning, TEXT("UTTTPlayerHUD::Setup - RowInfoClass is null"));
		return;
	}
	UIInfoChains_Box->ClearChildren();
	
	const int32 CountFigures = static_cast<uint8>(EFigureType::Max);
	for(int i = 0; i < CountFigures; ++i)
	{
		UTTTPlayerInfoRow* InfoRow = CreateWidget<UTTTPlayerInfoRow>(this, RowInfoClass);

		if (!InfoRow)
		{
			break;
		}

		const EFigureType FigureType = static_cast<EFigureType>(i);
		InfoRow->Setup(FigureType);
		UIInfoChains_Box->AddChild(InfoRow);
		InfoRows.Add(FigureType, InfoRow);
	}

	GameMode->GetTurnManager()->OnChangeCountChains.AddUObject(this, &UTTTPlayerHUD::ChangeCountChains);
}

void UTTTPlayerHUD::ChangeCountChains(EFigureType FigureType, int32 Count3Chains, int32 Count4Chains)
{
	if (InfoRows.Contains(FigureType))
	{
		InfoRows[FigureType]->ChangeCountChains(Count3Chains, Count4Chains);
	}
}
