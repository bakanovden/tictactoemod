// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/TTTSelectFigureMenu.h"

#include "Components/Button.h"
#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTGameplayGameMode.h"

bool UTTTSelectFigureMenu::Initialize()
{
	const bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	if (!SelectFigureSquare_Button)
	{
		return false;
	}
	SelectFigureSquare_Button->OnClicked.AddDynamic(this, &UTTTSelectFigureMenu::ClickSelectSquare);

	if (!SelectFigureCircle_Button)
	{
		return false;
	}
	SelectFigureCircle_Button->OnClicked.AddDynamic(this, &UTTTSelectFigureMenu::ClickSelectCircle);

	if (!SelectFigureCross_Button)
	{
		return false;
	}
	SelectFigureCross_Button->OnClicked.AddDynamic(this, &UTTTSelectFigureMenu::ClickSelectCross);

	if (!SelectFigureRandom_Button)
	{
		return false;
	}
	SelectFigureRandom_Button->OnClicked.AddDynamic(this, &UTTTSelectFigureMenu::ClickSelectRandom);

	return true;
}

void UTTTSelectFigureMenu::SetGameMode(ATTTGameplayGameMode* GameplayGameMode)
{
	GameMode = GameplayGameMode;
}

void UTTTSelectFigureMenu::ClickSelectSquare()
{
	SelectFigure(EFigureType::Square);
}

void UTTTSelectFigureMenu::ClickSelectCircle()
{
	SelectFigure(EFigureType::Circle);
}

void UTTTSelectFigureMenu::ClickSelectCross()
{
	SelectFigure(EFigureType::Cross);
}

void UTTTSelectFigureMenu::ClickSelectRandom()
{
	const uint8 CountFigures = static_cast<uint8>(EFigureType::Max);
	uint8 RandomFigureIndex = FMath::RandRange(0, CountFigures - 1);
	SelectFigure(static_cast<EFigureType>(RandomFigureIndex));
}

void UTTTSelectFigureMenu::SelectFigure(EFigureType FigureType)
{
	GameMode->PlayerSelectedFigure(FigureType);
}
