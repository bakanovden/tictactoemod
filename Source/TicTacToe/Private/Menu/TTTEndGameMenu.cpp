// Fill out your copyright notice in the Description page of Project Settings.


#include "Menu/TTTEndGameMenu.h"

#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "GameFramework/GameModeBase.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Kismet/GameplayStatics.h"

bool UTTTEndGameMenu::Initialize()
{
	const bool bSuccess = Super::Initialize();
	if (!bSuccess)
	{
		return false;
	}

	if (!Play_Button)
	{
		return false;
	}
	Play_Button->OnClicked.AddDynamic(this, &UTTTEndGameMenu::Play);

	if (!Quit_Button)
	{
		return false;
	}
	Quit_Button->OnClicked.AddDynamic(this, &UTTTEndGameMenu::Quit);

	if (!Hide_Button)
	{
		return false;
	}
	Hide_Button->OnClicked.AddDynamic(this, &UTTTEndGameMenu::Hide);

	return true;
}

void UTTTEndGameMenu::SetupPlayerWin()
{
	Setup();
	EngGameStatus_Text->SetText(FText::FromString(TEXT("Win!")));
}

void UTTTEndGameMenu::SetupAIWin(const FString& WinnerName)
{
	Setup();
	EngGameStatus_Text->SetText(FText::FromString(FString::Printf(TEXT("Lose! %s win!"), *WinnerName)));
}

void UTTTEndGameMenu::SetupDraw()
{
	Setup();
	EngGameStatus_Text->SetText(FText::FromString(TEXT("Draw!")));
}

void UTTTEndGameMenu::SetGameMode(ATTTGameplayGameMode* NewGameMode)
{
	GameMode = NewGameMode;
}

void UTTTEndGameMenu::Play()
{
	GameMode->NewGame();
}

void UTTTEndGameMenu::Quit()
{
	APlayerController* PlayerController = GetWorld()->GetFirstPlayerController();
	if (!PlayerController)
	{
		return;
	}

	PlayerController->ConsoleCommand("quit");
}

void UTTTEndGameMenu::Hide()
{
	SetVisibility(ESlateVisibility::Hidden);
}
