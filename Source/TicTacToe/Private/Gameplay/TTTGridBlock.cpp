// Copyright Epic Games, Inc. All Rights Reserved.

#include "Gameplay/TTTGridBlock.h"
#include "Gameplay/TTTGrid.h"
#include "Components/StaticMeshComponent.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Gameplay/TTTTurnManager.h"

ATTTGridBlock::ATTTGridBlock()
{
	PrimaryActorTick.bCanEverTick = false;

	DefaultScene = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultScene"));
	RootComponent = DefaultScene;

	BlockMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockMesh"));
	BlockMesh->SetupAttachment(DefaultScene);

	bIsLock = false;
}

void ATTTGridBlock::BlockClicked(UPrimitiveComponent* ClickedComp, FKey ButtonClicked)
{
	if(GameMode && GameMode->GetTurnManager() && GameMode->GetTurnManager()->GetIsPlayerMove())
	{
		if (LockBlock(GameMode->GetPlayerFigure()))
		{
			GameMode->PlayerEndTurn();
		}
	}
}

bool ATTTGridBlock::LockBlock(EFigureType FigureType)
{
	if (!bIsLock)
	{
		bIsLock = true;

		if (OwningGrid != nullptr)
		{
			LockFigureType = FigureType;
			OwningGrid->LockBlock(FigureType, BlockIndex);

			BlockMesh->SetStaticMesh(MeshesByFigureType[LockFigureType]);
			BlockMesh->SetMaterial(0, HighlightNotAvailable);
			return true;
		}
	}

	return false;
}

void ATTTGridBlock::HighlightOn()
{
	if (!bIsLock)
	{
		BlockMesh->SetMaterial(0, HighlightAvailable);
	}
	else
	{
		BlockMesh->SetMaterial(0, HighlightNotAvailable);
	}
}

void ATTTGridBlock::HighlightOff()
{
	if (!bIsLock)
	{
		BlockMesh->SetMaterial(0, BaseMaterial);
	}
}

void ATTTGridBlock::SetBlockIndex(int32 NewBlockIndex)
{
	BlockIndex = NewBlockIndex;
}

void ATTTGridBlock::SetOwningGrid(ATTTGrid* NewOwningGrid)
{
	OwningGrid = NewOwningGrid;
}

void ATTTGridBlock::SetGameMode(ATTTGameplayGameMode* NewGameMode)
{
	GameMode = NewGameMode;
}

void ATTTGridBlock::BeginPlay()
{
	Super::BeginPlay();

	BlockMesh->SetMaterial(0, BaseMaterial);
	BlockMesh->OnClicked.AddDynamic(this, &ATTTGridBlock::BlockClicked);
}
