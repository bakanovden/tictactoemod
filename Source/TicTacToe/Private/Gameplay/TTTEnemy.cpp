#include "Gameplay/TTTEnemy.h"

#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Gameplay/TTTGridBlock.h"
#include "Gameplay/TTTGrid.h"

constexpr static int32 MIN_SIZE_FOR_INTERFERE = 3;

ATTTEnemy::ATTTEnemy()
{
	PrimaryActorTick.bCanEverTick = false;

	SelectedBlockIndex = -1;
	SelectedDirection = EDirection::Up;
	ChanceForMoveOnSelectedLine = 0.5f;
	ChanceInterferePlayer = 0.8f;
}

void ATTTEnemy::SetFigureType(EFigureType NewFigureType)
{
	FigureType = NewFigureType;
}

void ATTTEnemy::Move()
{
	if (!CheckAvailableSelectedLine())
	{
		FindNewLine();
	}

	if (SelectedBlockIndex == -1)
	{
		return;
	}

	if (!TryInterferePlayer())
	{
		if (ChanceForMoveOnSelectedLine > 0.0f && ChanceForMoveOnSelectedLine >= FMath::FRandRange(0.0f, 1.0f) &&
			AvailableBlocks.Num() > 0)
		{
			const int32 RandBlockIndex = FMath::RandRange(0, AvailableBlocks.Num() - 1);
			Grid->GetFreeBlock(AvailableBlocks[RandBlockIndex])->LockBlock(FigureType);
			AvailableBlocks.RemoveAt(RandBlockIndex);
		}
		else
		{
			RandomPlaceFigure();
		}
	}
}

void ATTTEnemy::SetGrid(ATTTGrid* NewGrid)
{
	Grid = NewGrid;
}

void ATTTEnemy::SetGameMode(ATTTGameplayGameMode* NewGameMode)
{
	GameMode = NewGameMode;
}

void ATTTEnemy::BeginPlay()
{
	Super::BeginPlay();
}

bool ATTTEnemy::TryInterferePlayer()
{
	if (ChanceInterferePlayer > 0.0f && ChanceInterferePlayer < FMath::FRandRange(0.0f, 1.0f))
	{
		return false;
	}

	const EFigureType CheckFigure = GameMode->GetPlayerFigure();
	const int32 LastIndex = Grid->GetSize() * Grid->GetSize();

	int32 TargetStartIndex = -1;
	EDirection TargetDirection = EDirection::Up;
	bool bIsFinded = false;
	for (int BlockIndex = 0; BlockIndex < LastIndex; ++BlockIndex)
	{
		for (const EDirection& Direction : Grid->UniqueDirection)
		{
			const int32 SumInDirection = Grid->GetSumLockedBlocks(BlockIndex, Direction, CheckFigure);
			const int32 SumFreeInDirection = Grid->GetSumFreeBlocksOrLockFigure(BlockIndex, Direction, CheckFigure);
			const int32 SumFreeInOppositeDirection = Grid->GetSumFreeBlocksOrLockFigure(BlockIndex, Grid->OppositeDirections[Direction], CheckFigure);
			const int32 SomPossiblePositions = SumFreeInDirection + SumInDirection + SumFreeInOppositeDirection;
			if (SumInDirection >= MIN_SIZE_FOR_INTERFERE && SomPossiblePositions >= GameMode->GetCountBlockForWin())
			{
				TargetStartIndex = BlockIndex;
				TargetDirection = Direction;
				bIsFinded = true;
				break;
			}
		}
		if (bIsFinded)
		{
			break;
		}
	}

	if (bIsFinded)
	{
		// move to the middle towards TargetDirection
		int32 DeltaX, DeltaY, X, Y;
		Grid->DeltaCoordinatesForDirection(TargetDirection, DeltaX, DeltaY);
		Grid->CoordinatesFromIndex(TargetStartIndex, X, Y);
		X += DeltaX * 2;
		Y += DeltaY * 2;
		Grid->IndexFromCoordinates(TargetStartIndex, X, Y);
		TArray<int32> AvailablePositions;
		// getting free cells in both directions
		GetAllAvailablePositionsFromMiddle(TargetStartIndex, TargetDirection, AvailablePositions);
		GetAllAvailablePositionsFromMiddle(TargetStartIndex, Grid->OppositeDirections[TargetDirection],
		                                   AvailablePositions);

		// Lock Block
		if (AvailablePositions.Num() > 0)
		{
			const int32 RandArrBlockIndex = FMath::RandRange(0, AvailablePositions.Num() - 1);
			const int32 RandBlockIndex = AvailablePositions[RandArrBlockIndex];
			Grid->GetFreeBlock(RandBlockIndex)->LockBlock(FigureType);
			return true;
		}
	}
	return false;
}

void ATTTEnemy::RandomPlaceFigure()
{
	ATTTGridBlock* RandomFreeBlock = Grid->GetRandomFreeBlock();
	RandomFreeBlock->LockBlock(FigureType);
	AvailableBlocks.Remove(RandomFreeBlock->GetBlockIndex());
}

bool ATTTEnemy::CheckAvailableSelectedLine()
{
	if (SelectedBlockIndex == -1)
	{
		return false;
	}

	for (const int32& BlockIndex : AvailableBlocks)
	{
		if (!Grid->BlockIsFree(BlockIndex))
		{
			return false;
		}
	}

	return true;
}

void ATTTEnemy::FindNewLine()
{
	AvailableBlocks.Empty();
	SelectedBlockIndex = -1;
	SelectedDirection = EDirection::Up;

	TArray<int32> AllPossibleIndexes;

	const int32 LastBlockIndex = Grid->GetSize() * Grid->GetSize();
	for (int32 BlockIndex = 0; BlockIndex < LastBlockIndex; ++BlockIndex)
	{
		if (CheckAvailableForAnyDirection(BlockIndex))
		{
			AllPossibleIndexes.Add(BlockIndex);
		}
	}

	if (AllPossibleIndexes.Num() < 1)
	{
		return;
	}

	const int32 RandArrBlockIndex = FMath::RandRange(0, AllPossibleIndexes.Num() - 1);
	SelectedBlockIndex = AllPossibleIndexes[RandArrBlockIndex];

	TArray<EDirection> AvailableDirections;
	GetAllAvailableDirections(SelectedBlockIndex, AvailableDirections);

	if (AvailableDirections.Num() < 1)
	{
		return;
	}

	const int32 RandArrDirectionIndex = FMath::RandRange(0, AvailableDirections.Num() - 1);
	SelectedDirection = AvailableDirections[RandArrDirectionIndex];

	int32 DeltaX, DeltaY;
	Grid->DeltaCoordinatesForDirection(SelectedDirection, DeltaX, DeltaY);
	GetAllAvailablePositions(SelectedBlockIndex, SelectedDirection, AvailableBlocks);
}

bool ATTTEnemy::CheckAvailableForAnyDirection(int32 BlockIndex)
{
	const int32 CountDirections = static_cast<int32>(EDirection::Max);
	for (int i = 0; i < CountDirections; ++i)
	{
		const EDirection Direction = static_cast<EDirection>(i);
		if (CheckAvailableForDirection(BlockIndex, Direction))
		{
			return true;
		}
	}

	return false;
}

bool ATTTEnemy::CheckAvailableForDirection(int32 BlockIndex, EDirection Direction)
{
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin();
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (!Grid->IndexFromCoordinates(CheckIndex, X, Y) || (!Grid->BlockIsFree(CheckIndex) && Grid->
			GetBlock(CheckIndex)->GetLockFigureType() != FigureType))
		{
			return false;
		}

		X += DeltaX;
		Y += DeltaY;
	}

	return true;
}

void ATTTEnemy::GetAllAvailableDirections(int32 BlockIndex, TArray<EDirection>& Direction)
{
	const int32 CountDirections = static_cast<int32>(EDirection::Max);
	for (int i = 0; i < CountDirections; ++i)
	{
		EDirection CheckDirection = static_cast<EDirection>(i);
		if (CheckAvailableForDirection(BlockIndex, CheckDirection))
		{
			Direction.Add(CheckDirection);
		}
	}
}

void ATTTEnemy::GetAllAvailablePositions(int32 BlockIndex, EDirection Direction,
                                         TArray<int32>& NewAvailableBlocks)
{
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin();
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (Grid->IndexFromCoordinates(CheckIndex, X, Y) && Grid->BlockIsFree(CheckIndex))
		{
			NewAvailableBlocks.Add(CheckIndex);
		}

		X += DeltaX;
		Y += DeltaY;
	}
}

void ATTTEnemy::GetAllAvailablePositionsFromMiddle(int32 BlockIndex, EDirection Direction,
                                                   TArray<int32>& NewAvailableBlocks)
{
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin() / 2 + 2;
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (Grid->IndexFromCoordinates(CheckIndex, X, Y) && Grid->BlockIsFree(CheckIndex))
		{
			NewAvailableBlocks.Add(CheckIndex);
		}

		X += DeltaX;
		Y += DeltaY;
	}
}
