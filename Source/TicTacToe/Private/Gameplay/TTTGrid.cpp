#include "Gameplay/TTTGrid.h"
#include "Gameplay/TTTGridBlock.h"
#include "Engine/World.h"
#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogGrid, All, All);

ATTTGrid::ATTTGrid()
{
	PrimaryActorTick.bCanEverTick = false;

	DefaultRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRoot"));
	RootComponent = DefaultRoot;

	Size = 10;
	BlockSpacing = 300.0f;

	BlockClass = ATTTGridBlock::StaticClass();

	UniqueDirection.Add(EDirection::UpLeft);
	UniqueDirection.Add(EDirection::UpRight);
	UniqueDirection.Add(EDirection::Up);
	UniqueDirection.Add(EDirection::Right);

	OppositeDirections.Add(EDirection::UpLeft, EDirection::DownRight);
	OppositeDirections.Add(EDirection::UpRight, EDirection::DownLeft);
	OppositeDirections.Add(EDirection::Up, EDirection::Down);
	OppositeDirections.Add(EDirection::Right, EDirection::Left);
	OppositeDirections.Add(EDirection::DownRight, EDirection::UpLeft);
	OppositeDirections.Add(EDirection::DownLeft, EDirection::UpRight);
	OppositeDirections.Add(EDirection::Down, EDirection::Up);
	OppositeDirections.Add(EDirection::Left, EDirection::Right);
}


void ATTTGrid::LockBlock(EFigureType FigureType, int32 BlockIndex)
{
	if (FreeBlocks.Contains(BlockIndex))
	{
		LastLockedBlock = FreeBlocks[BlockIndex];
		FreeBlocks.Remove(BlockIndex);
	}
}

bool ATTTGrid::BlockIsFree(int32 BlockIndex)
{
	if(FreeBlocks.Contains(BlockIndex))
	{
		return FreeBlocks[BlockIndex] && !FreeBlocks[BlockIndex]->GetIsLock();
	}
	return false;	
}

void ATTTGrid::GetBlocks(TMap<int32, ATTTGridBlock*>& Blocks)
{
	Blocks = AllBlocks;
}

bool ATTTGrid::CoordinatesFromIndex(int32 BlockIndex, int32& X, int32& Y)
{
	X = BlockIndex / Size;
	Y = BlockIndex % Size;

	return BlockIndex >= 0 && BlockIndex < Size * Size;
}

bool ATTTGrid::IndexFromCoordinates(int32& BlockIndex, int32 X, int32 Y)
{
	BlockIndex = X * Size + Y;

	return (X >= 0 && X < Size) && (Y >= 0 && Y < Size);
}

void ATTTGrid::DeltaCoordinatesForDirection(EDirection Direction, int32& DeltaX, int32& DeltaY)
{
	switch (Direction)
	{
	case EDirection::Up:
		DeltaY = 0;
		DeltaX = 1;
		break;
	case EDirection::Down:
		DeltaY = 0;
		DeltaX = -1;
		break;
	case EDirection::Left:
		DeltaY = -1;
		DeltaX = 0;
		break;
	case EDirection::Right:
		DeltaY = 1;
		DeltaX = 0;
		break;
	case EDirection::UpRight:
		DeltaY = 1;
		DeltaX = 1;
		break;
	case EDirection::DownLeft:
		DeltaY = -1;
		DeltaX = -1;
		break;
	case EDirection::UpLeft:
		DeltaY = -1;
		DeltaX = 1;
		break;
	case EDirection::DownRight:
		DeltaY = 1;
		DeltaX = -1;
		break;
	case EDirection::Max:
	default:
		UE_LOG(LogGrid, Warning, TEXT("ATicTacToeBlockGrid::DeltaCoordinatesForDirection - Failed Directions"));
		break;
	}
}

ATTTGridBlock* ATTTGrid::GetRandomFreeBlock()
{
	TArray<int32> FreeBlockKeys;
	FreeBlocks.GetKeys(FreeBlockKeys);
	const int32 RandomIndex = FMath::RandRange(0, FreeBlockKeys.Num() - 1);
	return FreeBlocks[FreeBlockKeys[RandomIndex]];
}

ATTTGridBlock* ATTTGrid::GetFreeBlock(int32 BlockIndex)
{
	return FreeBlocks[BlockIndex];
}

ATTTGridBlock* ATTTGrid::GetBlock(int32 BlockIndex)
{
	return AllBlocks[BlockIndex];
}

ATTTGridBlock* ATTTGrid::GetLastLockedBlock()
{
	return LastLockedBlock;
}

int32 ATTTGrid::GetSumLockedBlocks(int32 BlockIndex, EDirection Direction, EFigureType FigureType)
{
	int32 Result = 0;
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin();
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (IndexFromCoordinates(CheckIndex, X, Y))
		{
			ATTTGridBlock* TicTacToeBlock = GetBlock(CheckIndex);
			if (TicTacToeBlock && TicTacToeBlock->GetIsLock() && TicTacToeBlock->GetLockFigureType() == FigureType)
			{
				++Result;
			}
			else
			{
				return Result;
			}
		}

		X += DeltaX;
		Y += DeltaY;
	}

	return Result;
}

int32 ATTTGrid::GetSumFreeBlocksOrLockFigure(int32 BlockIndex, EDirection Direction, EFigureType FigureType)
{
	int32 Result = 0;
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin();
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (IndexFromCoordinates(CheckIndex, X, Y))
		{
			ATTTGridBlock* TicTacToeBlock = GetBlock(CheckIndex);
			if (TicTacToeBlock && (!TicTacToeBlock->GetIsLock() || TicTacToeBlock->GetLockFigureType() == FigureType))
			{
				++Result;
			}
			else
			{
				return Result;
			}
		}

		X += DeltaX;
		Y += DeltaY;
	}

	return Result;
}

int32 ATTTGrid::GetSumLockedBlocksIgnoreSelf(int32 BlockIndex, EDirection Direction, EFigureType FigureType)
{
	const int32 Result = GetSumLockedBlocks(BlockIndex, Direction, FigureType);
	
	return Result > 0 ? Result - 1 : 0;
}

void ATTTGrid::BeginPlay()
{
	Super::BeginPlay();
	if(!GetWorld())
	{
		UE_LOG(LogGrid, Warning, TEXT("ATTTGrid::BeginPlay - GetWorld() failed"));
		return;
	}

	GameMode = Cast<ATTTGameplayGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	GameMode->SetGrid(this);

	if (!GameMode)
	{
		UE_LOG(LogGrid, Warning, TEXT("ATTTGrid::BeginPlay - UGameplayStatics::GetGameMode(GetWorld()) failed"));
		return;
	}

	const int32 NumBlocks = Size * Size;

	for (int32 BlockIndex = 0; BlockIndex < NumBlocks; BlockIndex++)
	{
		const float XOffset = (BlockIndex / Size) * BlockSpacing;
		const float YOffset = (BlockIndex % Size) * BlockSpacing;

		const FVector BlockLocation = FVector(XOffset, YOffset, 0.0f) + GetActorLocation();

		ATTTGridBlock* NewBlock = GetWorld()->SpawnActor<ATTTGridBlock>(BlockClass, BlockLocation, FRotator::ZeroRotator);

		if (NewBlock != nullptr)
		{
			NewBlock->SetOwningGrid(this);
			NewBlock->SetGameMode(GameMode);
			NewBlock->SetBlockIndex(BlockIndex);
			FreeBlocks.Add(BlockIndex, NewBlock);
			AllBlocks.Add(BlockIndex, NewBlock);
		}
	}
}
