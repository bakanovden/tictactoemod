#include "Gameplay/TTTPlayerController.h"

ATTTPlayerController::ATTTPlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
