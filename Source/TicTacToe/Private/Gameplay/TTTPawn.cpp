// Copyright Epic Games, Inc. All Rights Reserved.

#include "Gameplay/TTTPawn.h"
#include "Gameplay/TTTGridBlock.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/PlayerController.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Kismet/GameplayStatics.h"

ATTTPawn::ATTTPawn(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	CurrentBlockFocus = nullptr;
	PlayerController = nullptr;

	bCanMove = false;
}

void ATTTPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (PlayerController)
	{
		FVector Start, Dir, End;
		PlayerController->DeprojectMousePositionToWorld(Start, Dir);
		End = Start + (Dir * 8000.0f);
		TraceForBlock(Start, End, false);
	}
}

void ATTTPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("OpenEndGameMenu"), IE_Pressed, this, &ATTTPawn::OpenEndGameMenu);
	PlayerInputComponent->BindAction(TEXT("Test2"), IE_Pressed, this, &ATTTPawn::OpenEndGameMenu);
}

void ATTTPawn::CalcCamera(float DeltaTime, struct FMinimalViewInfo& OutResult)
{
	Super::CalcCamera(DeltaTime, OutResult);

	OutResult.Rotation = FRotator(-90.0f, -90.0f, 0.0f);
}

void ATTTPawn::StartTurn()
{
	bCanMove = true;
}

void ATTTPawn::EndTurn()
{
	bCanMove = false;
}

void ATTTPawn::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<APlayerController>(GetController());
}

void ATTTPawn::TraceForBlock(const FVector& Start, const FVector& End, bool bDrawDebugHelpers)
{
	if(bCanMove)
	{
		FHitResult HitResult;
		GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECC_Visibility);
		if (bDrawDebugHelpers)
		{
			DrawDebugLine(GetWorld(), Start, HitResult.Location, FColor::Red);
			DrawDebugSolidBox(GetWorld(), HitResult.Location, FVector(20.0f), FColor::Red);
		}
		if (HitResult.Actor.IsValid() && HitResult.Actor->IsA(ATTTGridBlock::StaticClass()))
		{
			ATTTGridBlock* HitBlock = Cast<ATTTGridBlock>(HitResult.Actor.Get());
			if (CurrentBlockFocus != HitBlock)
			{
				if (CurrentBlockFocus)
				{
					CurrentBlockFocus->HighlightOff();
				}
				if (HitBlock)
				{
					HitBlock->HighlightOn();
				}
				CurrentBlockFocus = HitBlock;
			}
		}
		else if (CurrentBlockFocus)
		{
			CurrentBlockFocus->HighlightOff();
			CurrentBlockFocus = nullptr;
		}
	}
	else if (CurrentBlockFocus)
	{
		CurrentBlockFocus->HighlightOff();
		CurrentBlockFocus = nullptr;
	} 
}

void ATTTPawn::OpenEndGameMenu()
{
	ATTTGameplayGameMode* GameplayGameMode = Cast<ATTTGameplayGameMode>(UGameplayStatics::GetGameMode(this));

	GameplayGameMode->TryReopenEndGameMenu();
}
