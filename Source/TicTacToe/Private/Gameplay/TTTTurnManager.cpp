#include "Gameplay/TTTTurnManager.h"

#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTEnemy.h"
#include "Gameplay/TTTGameplayGameMode.h"
#include "Gameplay/TTTGridBlock.h"
#include "Gameplay/TTTGrid.h"
#include "Gameplay/TTTPawn.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY_STATIC(LogTurnManager, All, All);

ATTTTurnManager::ATTTTurnManager()
{
	PrimaryActorTick.bCanEverTick = false;

	OrderMoves.Add(EFigureType::Square);
	OrderMoves.Add(EFigureType::Circle);
	OrderMoves.Add(EFigureType::Cross);
}

void ATTTTurnManager::StartGame()
{
	CurrentMoveIndex = 0;
	NextMove();
}

void ATTTTurnManager::StartNextStageGame()
{
	if(!UpdateStageGame())
	{
		NextMove();
	}
}

void ATTTTurnManager::NextMove()
{
	if (PlayerFigure == OrderMoves[CurrentMoveIndex])
	{
		Player->StartTurn();
		bIsPlayerMove = true;
	}
	else
	{
		GetWorldTimerManager().SetTimer(AIMoveTimerHandle, this, &ATTTTurnManager::AIMove, 0.5f);
		bIsPlayerMove = false;
	}
}

bool ATTTTurnManager::UpdateStageGame()
{
	if (CheckWinGame())
	{
		GameMode->WinGame(OrderMoves[CurrentMoveIndex]);
		return true;
	}

	UpdateCountChains();
	CurrentMoveIndex = (CurrentMoveIndex + 1) % OrderMoves.Num();
	if (CurrentMoveIndex == 0)
	{
		if (CheckDrawGame())
		{
			GameMode->DrawGame();
			return true;
		}
	}

	return false;
}

void ATTTTurnManager::AddEnemy(EFigureType FigureType, ATTTEnemy* Enemy)
{
	Enemies.Add(FigureType, Enemy);
}

void ATTTTurnManager::SetGrid(ATTTGrid* NewGrid)
{
	Grid = NewGrid;
}

void ATTTTurnManager::SetGameMode(ATTTGameplayGameMode* NewGameMode)
{
	GameMode = NewGameMode;
}

void ATTTTurnManager::SetPlayerFigure(EFigureType NewPlayerFigure)
{
	PlayerFigure = NewPlayerFigure;
}

void ATTTTurnManager::BeginPlay()
{
	Super::BeginPlay();

	if (!GetWorld())
	{
		return;
	}

	Player = Cast<ATTTPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}

void ATTTTurnManager::AIMove()
{
	GetWorldTimerManager().ClearTimer(AIMoveTimerHandle);
	if (Enemies.Contains(OrderMoves[CurrentMoveIndex]))
	{
		Enemies[OrderMoves[CurrentMoveIndex]]->Move();
	}

	StartNextStageGame();
}

bool ATTTTurnManager::CheckWinGame()
{
	if (CheckWinGameInDirection(EDirection::Up))
	{
		return true;
	}

	if (CheckWinGameInDirection(EDirection::Left))
	{
		return true;
	}

	if (CheckWinGameInDirection(EDirection::UpLeft))
	{
		return true;
	}

	if (CheckWinGameInDirection(EDirection::UpRight))
	{
		return true;
	}

	return false;
}

bool ATTTTurnManager::CheckWinGameInDirection(EDirection Direction)
{
	ATTTGridBlock* LastLockedBlock = Grid->GetLastLockedBlock();
	int32 LastBlockIndex = LastLockedBlock->GetBlockIndex();
	EDirection OppositeDirection = Grid->OppositeDirections[Direction];

	const int32 DirectionLocked = Grid->GetSumLockedBlocksIgnoreSelf(LastBlockIndex, Direction, OrderMoves[CurrentMoveIndex]);
	const int32 OppositeDirectionLocked = Grid->GetSumLockedBlocksIgnoreSelf(LastBlockIndex, OppositeDirection, OrderMoves[CurrentMoveIndex]);
	if (DirectionLocked + OppositeDirectionLocked + 1 >= GameMode->GetCountBlockForWin())
	{
		return true;
	}

	return false;
}

void ATTTTurnManager::UpdateCountChains()
{
	const EFigureType CheckFigure = OrderMoves[CurrentMoveIndex];
	int32 Count3Chains = 0;
	int32 Count4Chains = 0;
	const int32 LastIndex = Grid->GetSize() * Grid->GetSize();

	TSet<FString> AlreadyChecked;
	for (int BlockIndex = 0; BlockIndex < LastIndex; ++BlockIndex)
	{
		for (const EDirection& Direction : Grid->UniqueDirection)
		{
			const int32 SumInDirection = Grid->GetSumLockedBlocks(BlockIndex, Direction, OrderMoves[CurrentMoveIndex]);

			if (!ContainsNamesFor4Chains(AlreadyChecked, BlockIndex, Direction))
			{
				if (SumInDirection == 3)
				{
					++Count3Chains;
				}
				else if (SumInDirection == 4)
				{
					++Count4Chains;
					AddNamesFor4Chains(AlreadyChecked, BlockIndex, Direction);
				}
			}
		}
	}
	OnChangeCountChains.Broadcast(CheckFigure, Count3Chains, Count4Chains);
}

bool ATTTTurnManager::ContainsNamesFor4Chains(const TSet<FString>& AlreadyChecked, int32 StartIndex,
                                              EDirection Direction)
{
	if (AlreadyChecked.Num() < 1)
	{
		return false;
	}

	//3 indexes
	TArray<int32> IndexesInDirection;
	int32 DeltaX, DeltaY, X, Y, CheckIndex = StartIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(StartIndex, X, Y);
	for (int32 i = 0; i < 3; ++i)
	{
		Grid->IndexFromCoordinates(CheckIndex, X, Y);
		IndexesInDirection.Add(CheckIndex);

		X += DeltaX;
		Y += DeltaY;
	}

	const FString ThreeIndexes = FString::Printf(TEXT("%s%s%s"), *FString::FromInt(IndexesInDirection[0]),
	                                             *FString::FromInt(IndexesInDirection[1]),
	                                             *FString::FromInt(IndexesInDirection[2]));
	return AlreadyChecked.Contains(ThreeIndexes);
}

void ATTTTurnManager::AddNamesFor4Chains(TSet<FString>& AlreadyChecked, int32 StartIndex, EDirection Direction)
{
	//4 indexes
	TArray<int32> IndexesInDirection;
	int32 DeltaX, DeltaY, X, Y, CheckIndex = StartIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(StartIndex, X, Y);
	for (int32 i = 0; i < 4; ++i)
	{
		Grid->IndexFromCoordinates(CheckIndex, X, Y);
		IndexesInDirection.Add(CheckIndex);

		X += DeltaX;
		Y += DeltaY;
	}

	const FString ThreeIndexes = FString::Printf(TEXT("%s%s%s"), *FString::FromInt(IndexesInDirection[1]),
	                                             *FString::FromInt(IndexesInDirection[2]),
	                                             *FString::FromInt(IndexesInDirection[3]));
	const FString FourIndexes = FString::Printf(TEXT("%s%s"), *FString::FromInt(IndexesInDirection[0]), *ThreeIndexes);
	AlreadyChecked.Add(ThreeIndexes);
	AlreadyChecked.Add(FourIndexes);
}

bool ATTTTurnManager::CheckDrawGame()
{
	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckDrawGame Start"));
	TMap<int32, ATTTGridBlock*> Blocks;
	Grid->GetBlocks(Blocks);

	TArray<int32> BlockIndexes;
	TArray<EFigureType> AvailableTurnsFigures;
	Blocks.GetKeys(BlockIndexes);
	for (const int32& BlockIndex : BlockIndexes)
	{
		ATTTGridBlock* TicTacToeBlock = Grid->GetBlock(BlockIndex);
		const bool bIsFullLine = CheckAvailableForFigureTypeAnyDirection(TicTacToeBlock->GetLockFigureType(), BlockIndex);
		if (TicTacToeBlock && TicTacToeBlock->GetIsLock() && bIsFullLine)
		{
			AvailableTurnsFigures.AddUnique(TicTacToeBlock->GetLockFigureType());
		}
	}

	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckDrawGame End"));
	return AvailableTurnsFigures.Num() == 0;
}

bool ATTTTurnManager::CheckAvailableForFigureTypeAnyDirection(EFigureType Figure, int32 BlockIndex)
{
	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableForFigureTypeAnyDirection Start"));
	const int32 CountDirections = static_cast<int32>(EDirection::Max);
	for (int i = 0; i < CountDirections; ++i)
	{
		if (CheckAvailableInDirectionForFigure(Figure, BlockIndex, static_cast<EDirection>(i)))
		{
			UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableForFigureTypeAnyDirection End true"));
			return true;
		}
	}
	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableForFigureTypeAnyDirection End false"));
	return false;
}

bool ATTTTurnManager::CheckAvailableInDirectionForFigure(EFigureType Figure, int32 BlockIndex, EDirection Direction)
{
	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableInDirectionForFigure Start"));
	const int32 CountBlockForWin = GameMode->GetCountBlockForWin();
	int32 DeltaX, DeltaY, X, Y, CheckIndex = BlockIndex;
	Grid->DeltaCoordinatesForDirection(Direction, DeltaX, DeltaY);
	Grid->CoordinatesFromIndex(BlockIndex, X, Y);
	for (int32 i = 0; i < CountBlockForWin; ++i)
	{
		if (!Grid->IndexFromCoordinates(CheckIndex, X, Y) || (!Grid->BlockIsFree(CheckIndex) && Grid->
			GetBlock(CheckIndex)->GetLockFigureType() != Figure))
		{
			UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableInDirectionForFigure End false"));
			return false;
		}

		X += DeltaX;
		Y += DeltaY;
	}

	UE_LOG(LogTurnManager, Display, TEXT("ATTTTurnManager::CheckAvailableInDirectionForFigure End true"));
	return true;
}
