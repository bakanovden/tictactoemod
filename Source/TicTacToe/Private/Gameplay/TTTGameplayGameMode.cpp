#include "Gameplay/TTTGameplayGameMode.h"

#include "FunctionLibrary/TTTGameplayTypes.h"
#include "Gameplay/TTTEnemy.h"
#include "Gameplay/TTTGrid.h"
#include "Gameplay/TTTPlayerController.h"
#include "Gameplay/TTTPawn.h"
#include "Gameplay/TTTTurnManager.h"
#include "Kismet/GameplayStatics.h"
#include "Menu/TTTEndGameMenu.h"
#include "Menu/TTTSelectFigureMenu.h"
#include "Menu/TTTPlayerHUD.h"

DEFINE_LOG_CATEGORY_STATIC(LogGameplayGameMode, All, All);

ATTTGameplayGameMode::ATTTGameplayGameMode()
{
	DefaultPawnClass = ATTTPawn::StaticClass();
	PlayerControllerClass = ATTTPlayerController::StaticClass();

	CountBlockForWin = 5;
}

void ATTTGameplayGameMode::PlayerSelectedFigure(EFigureType FigureType)
{
	if (SelectFigureMenu)
	{
		SelectFigureMenu->Teardown();
		SelectFigureMenu = nullptr;
	}

	PlayerFigure = FigureType;
	StartGame();
}

void ATTTGameplayGameMode::PlayerEndTurn()
{
	Player->EndTurn();
	TurnManager->StartNextStageGame();
}

void ATTTGameplayGameMode::StartGame()
{

	TurnManager = GetWorld()->SpawnActor<ATTTTurnManager>(FVector::ZeroVector, FRotator::ZeroRotator);
	if (!TurnManager)
	{
		UE_LOG(LogGameplayGameMode, Warning, TEXT("ATTTGameplayGameMode::StartGam - TurnManager is null"));
		return;
	}

	TurnManager->SetGameMode(this);
	TurnManager->SetGrid(Grid);
	TurnManager->SetPlayerFigure(PlayerFigure);

	if (!PlayerUIClass || !GetWorld() || !GetWorld()->GetFirstPlayerController())
	{
		UE_LOG(LogGameplayGameMode, Warning, TEXT("ATTTGameplayGameMode::StartGam - PlayerUIClass or GetWorld() or etWorld()->GetFirstPlayerController() is null"));
		return;
	}

	PlayerUI = CreateWidget<UTTTPlayerHUD>(GetWorld()->GetFirstPlayerController(), PlayerUIClass);
	PlayerUI->Setup(this);

	const int32 NumberFigures = static_cast<int32>(EFigureType::Max);  
	for (int32 i = 0; i < NumberFigures; ++i)
	{
		const EFigureType CurrentFigure = static_cast<EFigureType>(i);
		if (CurrentFigure != PlayerFigure)
		{
			ATTTEnemy* NewEnemy = GetWorld()->SpawnActor<ATTTEnemy>(FVector::ZeroVector, FRotator::ZeroRotator);
			NewEnemy->SetGrid(Grid);
			NewEnemy->SetGameMode(this);
			NewEnemy->SetFigureType(CurrentFigure);
			TurnManager->AddEnemy(CurrentFigure, NewEnemy);
		}
	}

	TurnManager->StartGame();
}

void ATTTGameplayGameMode::WinGame(EFigureType WinFigure)
{
	if (!EndGameMenuClass || !GetWorld() || !GetWorld()->GetFirstPlayerController())
		return;

	EndGameMenu = CreateWidget<UTTTEndGameMenu>(GetWorld()->GetFirstPlayerController(), EndGameMenuClass);

	if (!EndGameMenu)
		return;

	EndGameMenu->SetGameMode(this);
	if (WinFigure == PlayerFigure)
	{
		EndGameMenu->SetupPlayerWin();
	}
	else
	{
		FString WinnerName;
		UGameplayTypes::GetFigureTypeName(WinFigure, WinnerName);
		EndGameMenu->SetupAIWin(WinnerName);
	}
}

void ATTTGameplayGameMode::DrawGame()
{
	if (!EndGameMenuClass || !GetWorld() || !GetWorld()->GetFirstPlayerController())
		return;

	EndGameMenu = CreateWidget<UTTTEndGameMenu>(GetWorld()->GetFirstPlayerController(), EndGameMenuClass);

	if (!EndGameMenu)
		return;

	EndGameMenu->SetupDraw();
	EndGameMenu->SetGameMode(this);
}

void ATTTGameplayGameMode::TryReopenEndGameMenu()
{
	if (EndGameMenu && EndGameMenu->GetVisibility() == ESlateVisibility::Hidden)
	{
		EndGameMenu->SetVisibility(ESlateVisibility::Visible);
	}
}

void ATTTGameplayGameMode::NewGame()
{
	if (!GetWorld())
	{
		UE_LOG(LogGameplayGameMode, Warning, TEXT("AMainMenuGameMode::NewGame - Failed GetWrold()"));
	}

	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), GameLevel);
}

void ATTTGameplayGameMode::SetGrid(ATTTGrid* NewGrid)
{
	Grid = NewGrid;
}

void ATTTGameplayGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (!SelectFigureMenuClass || !GetWorld() || !GetWorld()->GetFirstPlayerController())
		return;

	SelectFigureMenu = CreateWidget<UTTTSelectFigureMenu>(GetWorld()->GetFirstPlayerController(), SelectFigureMenuClass);

	if (!SelectFigureMenu)
		return;

	SelectFigureMenu->Setup();

	SelectFigureMenu->SetGameMode(this);

	Player = Cast<ATTTPawn>(UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
}
