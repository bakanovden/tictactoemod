// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenu/TTTMainMenuGameMode.h"

#include "Blueprint/UserWidget.h"
#include "Gameplay/TTTPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Menu/TTTMainMenu.h"

DEFINE_LOG_CATEGORY_STATIC(LogMainMenuGameMode, All, All);

ATTTMainMenuGameMode::ATTTMainMenuGameMode()
{
	PlayerControllerClass = ATTTPlayerController::StaticClass();
}

void ATTTMainMenuGameMode::NewGame()
{
	if (MainMenu)
	{
		MainMenu->Teardown();
		MainMenu = nullptr;
	}

	if (!GetWorld())
	{
		UE_LOG(LogMainMenuGameMode, Warning, TEXT("AMainMenuGameMode::NewGame - Failed GetWrold()"));
	}

	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), GameLevel);
}

void ATTTMainMenuGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (!MainMenuClass || !GetWorld() || !GetWorld()->GetFirstPlayerController())
		return;

	MainMenu = CreateWidget<UTTTMainMenu>(GetWorld()->GetFirstPlayerController(), MainMenuClass);

	if (!MainMenu)
		return;

	MainMenu->Setup();

	MainMenu->SetGameMode(this);
}
