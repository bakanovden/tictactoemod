// Fill out your copyright notice in the Description page of Project Settings.


#include "FunctionLibrary/TTTGameplayTypes.h"

void UGameplayTypes::GetFigureTypeName(EFigureType Figure, FString& String)
{
	{
		switch(Figure)
		{
			case EFigureType::Square:
				String = TEXT("Square");
				break;
			case EFigureType::Circle:
				String = TEXT("Circle");
				break;
			case EFigureType::Cross:
				String = TEXT("Cross");
				break;
			default:
				String = TEXT("Undefined");
		}
	}
}
